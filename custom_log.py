# -*- coding: utf-8 -*-
#
# custom_log.py
#
# How to use it un iPython
# %run custom_log
# logger.debug('My message')  # warn, crit, etc.
#
# For any python console
# from custom_log import logger
# will also works
##

from __future__ import unicode_literals

__author__ = 'Jeremie Balagna-Ranin <jeremie.balagna@gmail.com>'

import logging

import sys

handler = logging.StreamHandler(sys.stdout)
formatter = logging.Formatter('[%(levelname)s] %(asctime)s -> %(name)s: %(message)s')
handler.setFormatter(formatter)

logger = logging.getLogger('root')
logger.setLevel(logging.DEBUG)
logger.addHandler(handler)

logging.addLevelName(logging.DEBUG, "\033[0;36m%s\033[1;0m" % logging.getLevelName(logging.DEBUG))
logging.addLevelName(logging.WARNING, "\033[0;33m%s\033[1;0m" % logging.getLevelName(logging.WARNING))
logging.addLevelName(logging.WARN, "\033[0;33m%s\033[1;0m" % logging.getLevelName(logging.WARN))
logging.addLevelName(logging.ERROR, "\033[1;31m%s\033[1;0m" % logging.getLevelName(logging.ERROR))
logging.addLevelName(logging.CRITICAL, "\033[7;31m%s\033[1;0m" % logging.getLevelName(logging.CRITICAL))
logging.addLevelName(logging.FATAL, "\033[7;31m%s\033[1;0m" % logging.getLevelName(logging.FATAL))
