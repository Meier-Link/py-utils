# -*- coding: utf-8 -*-

# How to change directory as a bash's cd
# Source: http://stackoverflow.com/questions/431684/how-do-i-cd-in-python/13197763#answer-13197763
# Bound for : Python 2.7.x and 3.x

from __future__ import print_function, with_statement
from os import getcwd, chdir
from os.path import expanduser, expandvars, abspath


class cd:
    """Context mana ger for changing the current working directory"""
    def __init__(self, new_path='./'):
        self.new_path = abspath(expanduser(expandvars(new_path)))

    def __enter__(self):
        self.saved_path = getcwd()
        chdir(self.new_path)

    def __exit__(self, etype, value, traceback):
        chdir(self.saved_path)

    def __call__(self, dest='./'):
        chdir(abspath(expanduser(expandvars(dest))))

if __name__ == '__main__':
    print("Just an example with ls command")

    import subprocess
    with cd("~/Pictures"):
        # We are in ~/Pictures
        subprocess.call("ls")

    # Now we are back to the previous directory

    # We can also run a simple cd following this way
    print("Change dir ...")
    subprocess.call('pwd')
    cd()('~/')  # Hum ok, strange syntax x)
    subprocess.call('pwd')
