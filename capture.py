# -*- coding: utf-8 -*-

# Source: http://stackoverflow.com/questions/16571150/how-to-capture-stdout-output-from-a-python-function-call#answer-16571630
# Very very useful when you call a function that print something to the stdout
# Bound for : Python 2.x

import sys
from cStringIO import StringIO

class Capturing(list):
    def __enter__(self):
        self._stdout = sys.stdout
        sys.stdout = self._stringio = StringIO()
        return self

    def __exit__(self, *args):
        self.extend(self._stringio.getvalue().splitlines())
        sys.stdout = self._stdout


if __name__ == '__main__':
    print "not captured"
    with Capturing() as output:
        print "captured"
    print "content was captured: {}".format(output)
    print "capture finished"
