# -*- coding: utf-8 -*-
#
# author: Meier Link <jeremie.balagna@autistici.org>
#
# Markdown to HTML
# ================
#
# Require: markdown
#
# Bound for: python 2.7 && 3.x

from __future__ import print_function, unicode_literals

import sys
import codecs
from os.path import isfile, splitext
from markdown import Markdown

class Md2html(object):
    tplt = """<!DOCTYPE html>
    <html>
    <head>
    <meta charset="UTF-8">
    <title>{title}</title>
    <link media="all" type="text/css" href="style.css" rel="stylesheet" />
    <body>
    {body}
    </body>
    </html>"""

    def __init__(self, mdfile, title=None):
        self.mdfile = mdfile
        self.title = title or splitext(mdfile)[0]

    def __str__(self):
        md = Markdown()
        ctt = codecs.open(self.mdfile, mode="r", encoding="utf8").read()
        ctt = md.convert(ctt)
        tplt = self.tplt.format(body=ctt, title='')
        return tplt

    def __unicode__(self):
        return self.__str__()

if __name__ == '__main__':
    mdfile = sys.argv[1]
    htmlfile = '{}.{}'.format(splitext(mdfile)[0], 'html')
    if isfile(mdfile):
        if len(sys.argv) > 2:
            title = sys.argv[2]
        else:
            title = None
        convert = Md2html(mdfile, title)
        dest = open(htmlfile, 'w')
        dest.write(unicode(convert))
        dest.close()
    else:
        exit('{} not found'.format(mdfile))
