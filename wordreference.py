# -*- coding: utf-8 -*-

from argparse import ArgumentParser

import urllib2
from HTMLParser import HTMLParser


class Config(dict):
    def __init__(self):
        self.description = "Get the Wordreference translation of your word"
        self.default_conf = {
                'inlang': 'en',
                'outlang': 'fr',
                'word': 'translation'
            }

    def from_cli(self):
        # Override from command line
        parser = ArgumentParser(description=self.description)
        for k in self.default_conf.keys():
            parser.add_argument('--{}'.format(k.replace('_', '-')))
        parser.set_defaults(**self)

        args_cfg = {k:v for k,v in vars(parser.parse_args()).items()}
        
        self.update((k, v) for k, v in args_cfg.iteritems() if v or None)

class WordReference(HTMLParser):
    def __init__(self, word, inlang='fr', outlang='en'):
        HTMLParser.__init__(self)

        self.wf = "http://www.wordreference.com"
        self.available = [
            'enes', 'enfr', 'enit', 'ende', 'enpt', 'esfr', 'espt', 'esen',
            'fren', 'iten', 'deen', 'pten', 'fres', 'ptes'
        ]
        self.ua = "Mozilla/5.0 (X11; U; Linux i686; en; rv:1.9.1.1) Gecko/20090715 Firefox/3.5.1"
        tr = "{}{}".format(inlang, outlang)
        if tr in self.available:
            self.tr = tr
        else:
            exit("Translation {} not available".format(tr))
        self.trin = inlang
        self.trout = outlang
        self.word = word
        self.results = {}
        self.handle = False
        self.ignore = False

    def handle_starttag(self, tag, attrs):
        attrs = dict(attrs)
        if tag == 'em' and 'tooltip' in attrs.get('class', ''):
            self.ignore = True
        if tag == 'tr':
            if self.tr in attrs.get('id', ''):
                self.handle = attrs.get('id')

    def handle_endtag(self, tag):
        if tag == 'em':
            self.ignore = False
        if tag == 'tr' and self.handle:
            self.handle = False

    def handle_data(self, data):
        if self.handle:
            if self.handle not in self.results.keys():
                self.results[self.handle] = []
            if not self.ignore:
                self.results[self.handle].append("{}".format(data))

    def url(self):
        return "{}/{}/{}".format(self.wf, self.tr, self.word)

    def run(self):
        req = urllib2.Request(self.url())
        req.add_header('User-Agent', self.ua)
        opener = urllib2.build_opener()
        cnn = opener.open(req)
        enc = cnn.headers.getparam('charset')
        #ctt = cnn.read().decode(enc)
        ctt = cnn.read()
        self.feed(ctt)
        print "{}\nresults:".format(self.url())
        if self.results:
            for k in self.results:
                intr = self.results[k][0]
                expl = self.results[k][1:]
                print "{0: <25} {1}".format(intr, ' '.join(expl))
        else:
            print "Nothing found..."

if __name__ == '__main__':
    cfg = Config()
    cfg.from_cli()
    if 'word' in cfg:
        cfg['word'] = cfg['word'].replace(' ', '+')
    wf = WordReference(**cfg)
    #wf = WordReference('slang', 'en', 'fr')
    wf.run()
