# -*- coding: utf-8 -*-

# We can manage sets (and lists)

# TODO idea: implement them in list/dict children to extend possibilities
# See: https://docs.python.org/2/library/operator.html

# Some lists to play with
a = [0, 1, 2, 3, 4, 5]
b = [4, 5, 6, 7, 8, 9]
c = [2, 3, 4]
print "a: {}; b: {}; c: {}".format(a, b, c)

intersection = list(set(a) & set(b))  # with 'and'
print "Intersection of a and b: {}".format(intersection)

difference = list(set(a) ^ set(b))  # with 'xor'
print "Difference of a and b: {}".format(difference)

union = list(set(a) | set(b))  # with 'or'
print "Union of a and b: {}".format(union)

update = set(a)
update ^= set(b) # with assignation
print "Update a with b: {}".format(list(update))

print "Is a contains c ? (with '>')"
if set(a) > set(c):
    print "a contains c"
else:
    print "a doesn't contain c"
