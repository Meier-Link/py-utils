# @file cidr.py
# @author jeremie.balagna@faries.space
# CIDR simple tools to showcase basic rules on CIDR manipulation.

from dataclasses import dataclass

class CIDRExcpetion(Exception):
    pass

@dataclass
class IPv4Range:
    '''Represent a IPv4 CIDR structure to be demonstrated.'''
    cidr: str

    def __ensure_valid_ip_against_range(self):
        '''Following the mask, check the given ip is valid.'''
        if self._ip[-1] % self.all_allowed_ips != 0:
            raise CIDRExcpetion("the ip part of the given CIDR doesn't fit in the standard format: not compatible with allowed ips by mask.")
        if self._mask <= 24 and (self._ip[-1] != 0 or self._ip[-2] % (self.all_allowed_ips % 255) != 0):
            raise CIDRExcpetion("the ip part of the given CIDR doesn't fit in the standard format: not compatible with allowed ips by mask.")
        if self._mask <= 16 and (self._ip[-1] != 0 or self._ip[-2] != 0 or self._ip[-3] % (self.all_allowed_ips % 255) != 0):
            raise CIDRExcpetion("the ip part of the given CIDR doesn't fit in the standard format: not compatible with allowed ips by mask.")


    def __post_init__(self):
        '''Ensuer the given CIDR respect basic RFC rules.'''
        try:
            self._ip = [int(x) for x in self.cidr.split('/')[0].split('.')]
            self._mask = int(self.cidr.split('/')[1])
        except ValueError as e:
            raise CIDRExcpetion(f"Some part of the given ip is not valid: {e}")
        if len(self._ip) != 4:
            raise CIDRExcpetion("the ip part of the given CIDR doesn't fit in the standard format: must contains 4 digits.")
        if not all(x <= 255 for x in self._ip):
            raise CIDRExcpetion("the ip part of the given CIDR doesn't fit in the standard format: grater than 255.")
        if not all(x >= 0 for x in self._ip):
            raise CIDRExcpetion("the ip part of the given CIDR doesn't fit in the standard format: lower than 0.")
        if self._mask < 8 or self._mask > 32:
            raise CIDRExcpetion("the mask part of the given CIDR doesn't fit in the standard format: out of the boundaries.")
        self.__ensure_valid_ip_against_range()

    def _join_ip(self, arr_ip):
        '''Go back to string version of the IP since we store it as a list of 4 ints.'''
        return '.'.join(str(x) for x in arr_ip)

    @property
    def ip(self):
        '''Return the stringified IP of the CIDR.'''
        return self._join_ip(self._ip)

    @property
    def mask(self):
        '''Return the mask part of the CIDR.'''
        return self._mask

    @property
    def all_allowed_ips(self):
        '''All IPs included in the mask, including first IPs and broadcast'''
        return 2**(32-self.mask)

    @property
    def allowed_ips(self):
        '''Allowed IPs applying restriction rules for net case.'''
        allowed_ips = self.all_allowed_ips - 4
        if allowed_ips < 1:
           raise CIDRExcpetion(f"Does not apply to the current CIDR: No allowed IP remains with a mask of {self.mask}.")
        return allowed_ips

    @property
    def _broadcast(self):
        num_frozen_bits = 4 - (self.mask / 8)
        int_num_bits = int(num_frozen_bits)
        if int_num_bits == num_frozen_bits:
            if int_num_bits > 0:  # Case mask /24, /16 or /8
                return self._ip[:-int_num_bits] + [255] * int_num_bits
            return self.ip  # Case mask /32
        if self.mask > 24:
            return self._ip[:-1] + [self._ip[-1], self.all_allowed_ips]
        if self.mask > 16:
            return self._ip[:-2] + [self.all_allowed_ips % 255, 255]
        return [self._ip[0], self.all_allowed_ips % 255, 255, 255]

    @property
    def broadcast(self):
        '''Calculate the braodcast address of this CIDR.'''
        return self._join_ip(self._broadcast)

    @property
    def reserved_ips(self):
        '''Show the reserved IPs following the use by Cloud provider
           (i.e. excluded network IP, broadcast, en reserved IPs for service purposes).
        '''
        if self.mask <= 30:
            ip0 = self._ip
            ip1 = ip0[:-1] + [ip0[-1] + 1]
            ip2 = ip1[:-1] + [ip1[-1] + 1]
            ip3 = ip2[:-1] + [ip2[-1] + 1]
            return [
                self._join_ip(ip0),
                self._join_ip(ip1),
                self._join_ip(ip2),
                self._join_ip(ip3),
                self.broadcast
            ]
        # Otherwise, not enough place for reserved IPs.
        return [
           self.ip,
           self.broadcast
        ]

    @property
    def _min_ip(self):
        return self._ip[:-1] + [self._ip[-1] + 3]

    @property
    def min_ip(self):
        '''Calculate the first usable IP taking forbidden ips into account.'''
        return self._join_ip(self._min_ip)

    @property
    def _max_ip(self):
        return self._broadcast[:-1] + [self._broadcast[-1] - 1]

    @property
    def max_ip(self):
        '''Calculate the last usable IP before the broadcast IP.'''
        return self._join_ip(self._max_ip)

    def is_in_range(self, ip):
        '''Determine if the given IP is inside the range of our CIDR.'''
        splitted = ip.split('.')
        if not len(splitted) == 4:
           raise CIDRException(f'The provided ip {ip} is not a valid ip: should be composed of 4 ints between 0 and 255 each.')
        try:
            splitted = [int(x) for x in splitted]
        except ValueError as e:
            raise CIDRExcpetion(f"Some part of the given ip is not valid: {e}")
        if not all(x <= 255 for x in splitted) and all(x >= 0 for x in splitted):
            raise CIDRExcpetion(f"the given ip {ip} has digits outside the 0 ~ 255 limits.")

        max_ip = [int(x) for x in self._broadcast[:-2]] + [int(self._broadcast[-1]) - 1]

        for d in range(4):
            if splitted[d] < self._min_ip[d] or splitted[d] > self._max_ip[d]:
                return False
        return True

    def contains_range(self, ip_range):
        '''Check if the given range is a subrange of the current one.'''
        tgt_max_ip = [int(x) for x in ip_range.max_ip.split('.')]
        tgt_min_ip = [int(x) for x in ip_range.min_ip.split('.')]
        for d in range(4):
            if tgt_min_ip[d] < self._min_ip[d] or tgt_max_ip[d] > self._max_ip[d]:
                return False
        return True
