# -*- coding: utf-8 -*-
#
# extractor.py
##

from __future__ import unicode_literals, absolute_import

__author__ = 'Jeremie Balagna-Ranin <jbalagna@neteven.com>'

from ..dicttree import DictTree


class Extractor(object):
    """Abstract class which must be used to extract data from source"""
    def __init__(self, name):
        self._tree = DictTree(name)
        self._errors = []

    def build_tree(self):
        raise NotImplementedError("This method must be overrided.")

    @property
    def result(self):
        return self._tree

    @property
    def errors(self):
        return self._errors
