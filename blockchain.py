#! /usr/bin/env python3

# @file blockchain.py
# @brief Simple yet working example of a blockchain implementation in Python.
# @author <jeremie.balagna@gmail.com> Jeremie Balagna-Ranin
# @licence WTFPL (http://www.wtfpl.net/)

# Resources:
# * https://medium.com/@lhartikk/a-blockchain-in-200-lines-of-code-963cc1cc0e54
# * https://towardsdatascience.com/building-a-minimal-blockchain-in-python-4f2e9934101d

import hashlib
import json
from time import time


class Block:
    """Represent a single block in the Blockchain."""
    def __init__(self, data, prev_block=None):
        """Create a raw new block.

            @param (str) data: the data carried by the block
            @param (Block) prev_block: the previous block on the chain.

            If prev_block is not provided, the created block is assumed to be
            the first of the chain.
        """
        self._idx = 0
        self._prev_hash = ''
        if prev_block:
            self._idx = prev_block.next_idx
            self._prev_hash = prev_block.hash
        self._time = time()
        self._data = data
        self._hash = Block.calc_hash(self)

    @property
    def idx(self):
        """The index of the block is currently a simple integer.

            @return (int) the index of the block.
        """
        return self._idx

    @property
    def next_idx(self):
        """Get the expected next index for the block which follow self.

            @return (int) the index of the next block.
        """
        return self._idx + 1

    @property
    def hash(self):
        """A simple way to get the hash. Currently do nothing but return
            self._hash.

            @return (str) the hexdigest of the block hash
        """
        return self._hash

    @property
    def prev_hash(self):
        """Convenient access to the hash of the previous node.

            @return (str) the hexdigest of the previous block hash
        """
        return self._prev_hash

    def __str__(self):
        """Represent the Block as a string for hashing purpose."""
        return str(self.idx) + self.prev_hash + str(self._time) + self._data

    @property
    def time(self):
        """Return a floating representation of the block time creation.

            @return (float)
        """
        return self._time

    @staticmethod
    def calc_hash(block):
        """Calculate the hash of the given block.

            @param (Block) block: For which block do we need to calculate the
            hash.
            @return (str) the hexdigest of the block hash.
        """
        return hashlib.sha256(str(block).encode('utf-8')).hexdigest()


class Blockchain:
    """A custom list which represent the current blockchain."""
    def __init__(self):
        """Create a raw new chain. It is automatically created with its genesis
            block."""
        self._chain = [Block(json.dumps(None))]

    def __len__(self):
        """Give the length of the chain as it's a list."""
        return len(self._chain)

    def __getitem__(self, idx):
        """Convenient shortcut to reach an item of the blockchain."""
        return self._chain[idx]

    def check_block(self, prev_block, cur_block):
        """Check the given block validity against the previous one.

            @param (Block) prev_block: the previous block on the chain.
            @param (Block) cur_block: the current block we have to check.
            @return (bool) True if the block is valid, False otherwise.

            Currently, the failure message is just printed to stdout (okay I'll
            do better next time).
        """
        if prev_block.next_idx != cur_block.idx:
            print("Invalid idx")
            return False
        if prev_block.hash != cur_block.prev_hash:
            print("Invalid previous hash")
            return False
        if prev_block.time > cur_block.time:
            print("Invalid date on new block.")
            return False
        if Block.calc_hash(cur_block) != cur_block.hash:
            print("Invalid current hash")
            return False
        return True

    def append(self, new_block):
        """Add a new block to the chain.

            @param (Block) new_block: the block to append to the chain.
            @return (bool) True if the block has been appened, False otherwise

            The addition will fail if the new block is not valid. (see
            Blockchain.check_block for more information.)
        """
        if self.check_block(self._chain[-1], new_block):
            self._chain.append(new_block)

    def check_chain(self, chain=None):
        """Check the full blockchain.

        @param (Blockchain) chain: The chain to check. If not given, it'll
        check self one.
        @return (bool) Return True if the chain is valid, False otherwise.

        It basically perform a check_block on each block of the chain.
        """
        if not chain:
            chain = self._chain
        for i in list(range(1, len(chain))):
            j = i + 1
            if j < len(chain) and not self.check_block(chain[-i], chain[-j]):
                return False
        return True


class Node:
    """Represent a node in the Blockchain implementation.

        Most of the example I seen on the net use here a webserver.
        I thought it's a little bit out of the scope of the blockchain study.
        So I chose to just give that class as an interface of the Blockchain
        API above.

        Here is a simple usage example:
        >>> from blockchain import Node
        >>> n1 = Node()
        >>> n2 = Node()
        >>> n2.mine({'answer': 42})
        >>> n.replace_chain(n2.chain)
    """
    def __init__(self):
        """Create a new node with its own Blockchain."""
        self.chain = Blockchain()

    def mine(self, data):
        """Generate a new block to the chain.

            @param (misc) data: The data to put in the block.

            NB: in this simplest implementation, data is expected to work with
            json.dumps to be stringified.
        """
        block = Block(json.dumps(data), self.chain[-1])
        self.chain.append(block)

    def replace_chain(self, new_chain):
        """Replace the current chain by the given one if it's valid and
            longer.

            @param (Blockchain) new_chain: The chain expected to replace the
                current one.
        """
        if self.chain.check_chain(new_chain) \
                and len(new_chain) > len(self.chain):
            self.chain = new_chain
