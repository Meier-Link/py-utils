Mettre en place Nginx et uWSGI pour servir un site web développé en Python
==========================================================================

Si, comme moi, vous avez enfin franchit le pas consistant à laisser PHP derrière vous pour passer à Python, voici quelque notes qui pourraient vous servir ...

Tout d'abord, mes sources
-------------------------

* [Vladikk's blog](http://vladikk.com/2013/09/12/serving-flask-with-nginx-on-ubuntu/)
* [Stackoverflow-0](http://vladikk.com/2013/09/12/serving-flask-with-nginx-on-ubuntu/)
* [Stackoverflow-1](https://blog.rudeotter.com/configure-flask-app-nginx-uwsgi/)

C'est ici que ça commence
-------------------------

Histoire de gagner un petit peu de temps, on va dire que vous avez pris l'exemple de la [page d'accueil](http://flask.pocoo.org/) de Flask.

Vous avez donc un site avec comme point d'entrée l'instance de Flask nommé "app".

### Flask tout seul.

C'est possible, mais ça ne sert que pour les tests. Donc, juste pour info, il suffit de faire

```
python hello.py
```

Pour voir un magnifique hello world dans votre navigateur favoris.

Mais pour faire sérieux, faut aller un peu plus loin.

### uWSGI

#### Démarrage

Si vous êtes comme moi, vous ferez un

```
pip install uwsgi
```

pour récupérer uWSGI.

**Remarque** : Il se peut que l'installation échoue parce que pip ne trouve
pas un certain "Python.h".<br/>
Dans ces cas là, il faut installer le paquet python-dev ("python-devel" sur les "Red Hat" like) et recommencer.

Ça entraîne quelques petites complications plus loin, mais rien de bien méchant :-)

Bien, maintenant que vous avez uWSGI sur votre machine, vous pouvez lancer votre Hello World comme suit :

```
uwsgi --http 127.0.0.1:3031 --wsgi-file hello.py --callable app
```

uWSGI propose un paquet d'autres options pour sa configuration, on choisit de faire simple, ici.

Et en parlant d'ici, nous, on doit pouvoir lancer uwsgi avec un fichier de configuration.

Comme on est gourmand et qu'on prévoit de faire plein de sites web, je vous propose de créer d'emblée un dossier /etc/uwsgi/apps-available où vous mettrez les fichiers ini de vos sites web, et un dossier /etc/uwsgi/apps-enabled pour les liens symboliques (vieux réflexe d'Apache, ok. Mais je trouve que c'est un bon principe de faire comme ça).

une fois les deux dossiers de créé, je vous donne mon exemple de fichier "hello.ini" :


```
[uwsgi]
#application's base folder
base = /home/user/vhosts/hello/app

#python module to import
app = hello
module = %(app)

#home = %(base)/venv # No venv
#home = %(base)
pythonpath = %(base)

#socket file's location
socket = /tmp/uwsgi.hello.socket

#permissions for the socket file
chmod-socket = 666

#the variable that holds a flask application inside the module imported
callable = app

#location of log files
logto = /home/user/vhosts/hello/logs/%n.log
```

Notez bien les points suivants :

* J'ai choisit de développer mes sites dans mon dossier persos, dans un ~/vhosts. À vous d'adapter selon vos habitudes. De plus, vous aurez peut-être un path particulier sur votre serveur, et le plus efficace est d'avoir à peu près la même chose en dev ...
* Le dossier de base du site est "hello", et j'ai mis le code python dans "hello/app"
* Je n'ai pas de virtualenv (venv), mais dans votre cas, ça peut servir
* La façon d'organiser les sockets vous appartient. Peut-être que /tmp/uwsgi/hello.sock aurait été mieux ?
* le chmod du socket n'est pas idéal niveau sécurité, je n'ai pas encore testé, mais ça doit marcher avec 644
* "callable" correspond au nom de l'instance de Flask dans votre hello.py
* Je met les logs du site dans un hello/logs

En résumé, il y a quelques choix qui sont tout à fait personnel et qui ne seront pas forcément les mêmes pour vous. Mais si vous modifiez quelque chose ici, n'oubliez pas d'adapter la suite ;-)

Quoi qu'il en soit, une fois que vous aurez créé le lien symbolique dans /etc/uwsgi/apps-enabled, vous pouvez lancer votre site web avec la commande suivante : 

```
uwsgi --ini /etc/uwsgi/apps-enabled/hello.ini
```

Notez bien qu'il y a peu de chance que vous voyez quoi que ce soit dans votre navigateur à cause de la ligne "socket" de la configuration. Je vous laisse regarder la doc de uWSGI pour l'utiliser seul.

#### Démon uWSGI

De façon ultra simpliste, uWSGI propose une option "--daemonize" qui prend un fichier où envoyer les logs en paramètre.<br>
Mais pour nous, ça ne suffit pas.

Le but étant que uwsgi redémarre au reboot de la machine, il faut également prévoir un fichier dans /etc/init.d/.

Les distros sympas vous fournissent un fichier "skeleton" dans /etc/init.d qui vous donne une base de départ (avec dedans les lignes spécifiques à votre distro).<br>
Je ne vais donc vous redonner que les lignes spécifiques à uWSGI


```
### BEGIN INIT INFO
# Provides:          uwsgi
# Required-Start:    $remote_fs $syslog
# Required-Stop:     $remote_fs $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Initscript for uWSGI
# Description:       This script will run .ini script found in 
#                    /etc/uwsgi/apps-enabled with uwsgi
### END INIT INFO

PATH=/sbin:/usr/sbin:/bin:/usr/bin
LOGTO=/var/log/uwsgi/emperor.log
ETC_PATH=/etc/uwsgi/apps-enabled
DESC="Run uWSGI"
NAME=uwsgi
DAEMON=/usr/local/bin/$NAME
DAEMON_ARGS="--master --emperor $ETC_PATH --die-on-term --uid www-data --gid www-data --daemonize $LOGTO"
PIDFILE=/var/run/$NAME.pid
SCRIPTNAME=/etc/init.d/$NAME
```

Sauvegardez ce script en /etc/init.d/uwsgi<br>
Vous pourrez l'utiliser ensuite comme n'importe quel script d'initialisation.

Quelques commentaires sur les options :

* 'master': dit à uWSGI d'utiliser un process master qui se charge de gérer les autres instances
* 'emperor' correspond au mode qui permet de gérer plusieurs process "vassaux" avec un empereur $ETC_PATH pointe vers le dossier où se trouvent les vassaux
* '--uid' et '--gid' indiquent le user et group utilisé par uwsgi. Notez que les fichiers de log doivent être la propriété de cet user/groupe
* '--daemonize' on en a déjà parlé

Notez que les "INIT INFO" et les variables sont importantes pour les init scripts (mais il peut y avoir quelques différences en fonction de votre GNU/Linux).

Maintenant, lancez votre serveur d'un simple /etc/init.d/uwsgi start et observez les fichiers de log ;-)

Enfin, pour qu'il se lance automatiquement au démarrage de votre serveur, il ne reste plus qu'à faire un 

```
update-rc.d uwsgi defaults
```

Et la magie opèrera au prochain reboot ;-)

### Nginx

Nginx se trouve dans les dépôts de toute distribution digne de ce nom.

Une fois installé, vous avez juste à ajouter le fichier de conf de votre site web dans /etc/nginx/sites-available/hello.localhost.com et créer son lien symbolique dans sites-enabled (eh oui, lui aussi fonctionne comme ça x)).

Je vous donne ici à titre d'exemple mon fichier de conf Nginx :

 
```
server {
  listen 8080;
  root /home/user/vhosts/hello/htdocs;
  index index.html;

  server_name hello.localhost.net;

  location / {
    try_files $uri $uri/ @app;
  }

  location @app {
    include uwsgi_params;
    uwsgi_pass unix:/tmp/uwsgi.hello.socket;
  }
}
```

Comme indiqué plus tôt, les infos doivent correspondre entre uWSGI et Nginx.<br>
Mais reprenons déjà dans l'ordre :

* listen indique le port d'écoute. Comme je n'ai pas encore viré Apache, Nginx écoute sur le 8080.
* root : Soyons honnête, je n'ai pas encore complètement viré les reliquats d'Apache ^^" Mais dans notre cas, le dossier htdocs contient un index.html que sera affiché seulement quand le user ira à la racine du site. Et en fait, ça m'arrange bien, car je compte dialoguer avec le serveur avec du javascript (du coup, première connexion, récupération du index.html qui charge css et js, et tout le reste est géré en js).
* index défini le fichier de base du site (vous pouvez en mettre plusieurs séparés par un espace)
* server_name : le nom que vous mettrez dans votre navigateur favoris :)
* location / défini le chemin par défaut. Ici il va essayer de récupérer le fichier correspondant au chemin, puis le même en rajoutant un "/" à la fin, et enfin il va aller rendre visite au bloc "app" qui suit ...
* localtion @app fait la connexion avec uwsgi :
    * include uwsgi_params permet de récupérer tout les paramètres par défaut pour dialoguer avec uwsgi
    * uwsgi_pass contient le chemin vers le socket (donc le même que celui de uWSGI, un socket étant un fichier d'échange ...)

Une fois ce fichier en place, il ne reste plus qu'à faire un /etc/init.d/nginx restart et 'normalement', tout fonctionne \o/