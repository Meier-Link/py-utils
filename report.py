#! /usr/bin/env python

from __future__ import unicode_literals

__author__ = 'Jeremie BR aka Meier Link <jeremie.balagna@gmail.com>'

import json

from datetime import datetime


class Report(object):
    def _byteify(self, data):
        """Make sure data are unicode."""
        if isinstance(data, dict):
            return {self._byteify(key): self._byteify(value) for key, value
                in data.iteritems()}
        elif isinstance(data, list) or isinstance(report, tuple):
            return [self._byteify(element) for element in data]
        elif isinstance(data, unicode):
            return data.encode('utf-8')
        else:
            return data

    def _convert(self, report):
        """Transform objects to dict and byteify strings.

        In case of native object, it'll explore it to byteify all string.
        Otherwise, it'll check all properties (i.e. methods with @property decorator)
        to find publicly available values.

        @return a dict which has dict instead of objects.
        """
        if report is None:
            return None

        if isinstance(report, int) or isinstance(report, str):
            return self._byteify(report)

        if isinstance(report, list) or isinstance(report, tuple):
            result = []
            for line in report:
                result.append(self._convert(line))
            if isinstance(report, tuple):
                return tuple(result)
            return result

        if isinstance(report, datetime):
            # Datetime doesn't use properties (and we may want custom format).
            return report.strftim(self._date_format)

        if isinstance(report, dict):
            result = {}
            for k, v in report.items():
                if isinstance(k, tuple):
                    k = self._tuple_concat.join(self._convert(k))
                else:
                    k = self._byteify(k)

                result[k] = self._convert(v)
            return result

        return {self._byteify(name): self._convert(val.fget(obj))
            for name, val in vars(obj.__class__).items()
            if isinstance(val, property)}

    def __init__(self, report, date_format=None, join_tuple_keys_with=None):
        self._date_format = date_format or '%Y-%m-%d %H:%M:%S'
        self._tuple_concat = join_tuple_keys_with or '.'
        self._report = self._convert(report)
        self._json = None
        self._xml = None
        self._html = None

    @property
    def json(self):
        """Export the report to json"""
        if not self._json:
            self._json = json.dumps(self._report, indent=2)
        return self._json

    @property
    def xml(self):
        """Export the report to xml"""
        if not self._xml:
            # TODO xmlification of the report
            self._xml = self._report
        return self._xml

    @property
    def html(self):
        """Export the report to html"""
        if not self._html:
            # TODO htmlification of the report
            self._html = self._report
        return self._html

    def export(self, dest, exp_format='json'):
        formats = {name: val for name, val in vars(ReferentielReport).items()
                if isinstance(val, property)}
        if exp_format not in formats.keys():
            raise TypeError('Allowed formats for export are {}'.format(formats))

        data = formats[exp_format].fget(self)
        dest = open(dest, 'w')
        dest.write(data)
        dest.close()
