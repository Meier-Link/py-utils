# -*- coding: utf-8 -*-

DEBUG = True
IMPORT_NAME = 'flask_site'
SECRET_KEY = 'development key'
USERNAME = 'admin'
PASSWORD = 'default'
SITE_TITLE = 'Demo Website'
