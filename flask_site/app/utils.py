# -*- coding: utf-8 -*-
#
# author:   Meier Link <jeremie.balagna@autistici.org>
# date:     19/10/2015
# version:  0.1
#
# utils.py
# ========
#
# Some tools which may be useful for website development

import inspect
from copy import copy
from itertools import izip_longest

from flask import jsonify, render_template

from conf import SITE_TITLE

def render_json(data, headers=None):
    """Return a response object which contain input data as a json object"""
    # Now it's just an example
    data = {'status': 200, 'data': data}
    r = jsonify(**data)
    if isinstance(headers, dict):
        r.headers.update(headers)
    return r

def render_stencil(tplt, kwargs={}):
    if 'title' not in kwargs.keys():
        kwargs['title'] = SITE_TITLE

    return render_template(tplt, **kwargs)

def getargs(argspec):
    """Parse argspec to get argspec with corresponding default value"""
    if argspec.defaults:
        args = argspec.args
        zipped = zip(args[::-1], argspec.defaults[::-1])[::-1]
        return args[:len(args)-len(zipped)] + zipped
        #return [(n, v) for n, v in izip_longest(
        #        argspec.args[::-1],
        #        argspec.defaults[::-1]
        #    )][::-1]
    return argspec.args

def build_routes(f, clsname=None):
    """Build Flask rules and set http method from function object

    Use the function to obtain the http method and rules with inspect

    :param f: the function object
    :param clsname: (optional) the class name if the function is a method
    """
    view_name = "_".join(f.__name__.split('_')[1:])
    method = f.__name__.split('_')[0].upper()
    prefix = '/{}'.format(view_name)
    if clsname:
        prefix = '/{}/{}'.format(clsname[:-4].lower(), view_name)

    args = getargs(inspect.getargspec(f))

    rules = [prefix]

    if args:
        i = 0
        for arg in args:
            a = arg
            if isinstance(arg, tuple):
                a, v = arg
                i += 1
                rules.append(rules[i-1])
            if a != 'self':
                rules[i] += '/<{}>'.format(a)
    return [{
        'rule': rule,
        'ep': rule[1:],
        'func': f,
        'meth': method
    } for rule in rules]

def isview(fname):
    http_verbs = ['GET', 'POST']
    if fname.split('_')[0].upper() in http_verbs:
        return True
    return False

