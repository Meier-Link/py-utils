# -*- coding: utf-8 -*-

from utils import *
from models import *
from decorators import *

from session import SessionView


def get_home(uname=None):
    name = uname or 'World'
    return render_json(u'Hello {}!'.format(name.capitalize()))


