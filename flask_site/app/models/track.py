#! /usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import

from datetime import datetime

from sqlalchemy import Column, ForeignKey, Integer, DateTime, String
from sqlalchemy.orm import relationship
from models.base import Base


class Track(Base):
    __tablename__ = 'tracks'
    id = Column(Integer, primary_key=True)
    date = Column(DateTime, nullable=False, default=datetime.now)
    token = Column(String(250), nullable=False)
    user_id = Column(Integer, ForeignKey('members.id'))
    ip = Column(String(100), default=None)
    user_agent = Column(String(250), default=None)

    user = relationship("members.id")

    def __repr__(self):
        return ("<Track (date={}, token={}, user={}, "
            "ip={}, user_agent={})>".format(
                self.date, self.token, self.user_id,
                self.ip, self.user_agent
            ))
