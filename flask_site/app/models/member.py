#! /usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import

from datetime import datetime

from sqlalchemy import Column, Integer, String, DateTime, Boolean
from models.base import Base


# TODO status significations (blacklisted, newsletter only, registred, ...)

class Member(Base):
    __tablename__ = 'members'
    id = Column(Integer, primary_key=True)
    pseudo = Column(String(250))
    register_date = Column(DateTime, nullable=False, default=datetime.now)
    email = Column(String(250), nullable=False)
    password = Column(String(250))
    # Used to allow the user to create a new password.
    token_val = Column(String(250), default=None)
    token_crea = Column(DateTime)
    # 0=blacklisted; 1=registred; ... (Can be customised).
    status = Column(Integer, nullable=False, default=1)
    # Useful if you have a newsletter.
    subscribed = Column(Boolean, nullable=False, default=True)

    def __repr__(self):
        return "<Member pseudo={}, email={}, password={})>".format(
                self.pseudo,
                self.email,
                self.password
            )
