# -*- coding: utf-8 -*-

import inspect
from datetime import datetime
from passlib.apps import custom_app_context as pwd_context

from flask import Flask, request

from utils import isview, build_routes

from models.base import db_session
from models import Track, Member


class App(Flask):
    """The class which override default Flask main class to add 'add_route'"""
    def add_routes(self, routes):
        for route in routes:
            if self.debug:
                print "[{}][DEBUG][{}] add {} to route {} {}".format(
                    str(datetime.today()),
                    self.import_name,
                    route['func'].__name__,
                    route.get('meth', 'GET'),
                    route['rule']
                )
            self.add_url_rule(
                route['rule'],
                endpoint=route['ep'],
                view_func=route['func'],
                methods=[route.get('meth', 'GET')]
            )


class View(object):
    """Base class for all views"""
    def __init__(self):
        self.__routes__ = None

    def build_routes(self):
        """Build routes for all methods"""
        meths = inspect.getmembers(self, predicate=inspect.ismethod)
        self_routes = []
        for mname, meth in meths:
            if isview(mname):
                self_routes += build_routes(meth, self.__class__.__name__)

        return  self_routes


class User(object):
    """Represent a connected user to the website.

    This class is instanciated when user is connected with a unique token which
    is associated to his session on his browser.
    The member will be associated to his token when he's log in.
    """
    def __init__(self):
        self._member = None
        self._user_ip = request.remote_addr
        self._token = None
        self._user_agent = "{browser}/{version} ({platform}) {lang}".format(
            browser=request.user_agent.browser,
            version=request.user_agent.version,
            platform=request.user_agent.platform,
            lang=request.user_agent.language or 'NC'
        )

    @property
    def is_authenticated(self):
        return self._member is not None

    @property
    def is_active(self):
        if self._member is not None:
            return self._member.status > 0
        return False

    @property
    def is_anonymous(self):
        pass

    @property
    def token(self):
        if self._token is None:
            self._token = 'gen token'  # TODO token creation
        return self._token

    def get_id(self):
        if self._member is not None:
            return unicode(self._member.id)
        return None

    def subscribe(self, email):
        """Register an email for the newsletter."""
        pass

    def register(self, member):
        """Register a new user."""
        db_session.add(member)
        db_session.commit()
        db_session.rollback()
        self._member = member

    def connect(self, login, password):
        """Connect user by its email as login."""
        member = db_session.query(Member).filter(Member.email==login).one()
        if not member:
            return False
        if pwd_context.verify(password, member.password):  #password_hash)
            self._member = member
            return True
        return False

    def track(user_id=None):
        ip = request.remote_addr
        token = None
        user_agent = "{browser}/{version} ({platform}) {lang}".format(
            browser=request.user_agent.browser,
            version=request.user_agent.version,
            platform=request.user_agent.platform,
            lang=request.user_agent.language or 'NC'
        )
        tracking = Track(
            token=token,
            user_id=user_id,
            ip=ip,
            user_agent=user_agent
        )
        db_session.add(tracking)
        db_session.commit()
        db_session.rollback()
