# -*- coding: utf-8 -*-
#
# author:   Meier Link <jeremie.balagna@autistici.org>
# date:     19/10/2015
# version:  0.1
#
# flask_site
# ==========
#
# A base for website projects with Flask
# This is not a guideline, just a way to do the things
#
# You are free to reuse it as-is.

from utils import isview, build_routes
import views
from core import App

from config import conf


import_name = conf.get('IMPORT_NAME', __name__)

app = App(import_name)
if conf.get('DEBUG'):
    app.debug = True
app.config.update(conf)

# Try to get view functions
view_functions = [getattr(views, f) for f in dir(views) if isview(f)]
for f in view_functions:
    app.add_routes(build_routes(f))

view_classes = [getattr(views, cls) for cls in dir(views) if 'View' in cls]
for cls in view_classes:
    app.add_routes(cls().routes())

if __name__ == '__main__':
    app.run()
