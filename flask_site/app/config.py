# -*- coding: utf-8 -*-

from os.path import dirname, abspath, join
from ConfigParser import RawConfigParser as ConfigParser


# Build conf from a file out side of the app/ dir
secure_path = abspath(join(dirname(__file__), '..', 'secure/app.cfg'))
config = ConfigParser()
config.read(secure_path)

bool_map = {'true': True, 'false': False}

conf = {k.upper(): bool_map[v.lower()] if v.lower() in bool_map.keys() else v
    for k, v in config.items('global')}
