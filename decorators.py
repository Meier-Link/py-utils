#! /usr/bin/env python
#
# decorators.py
#
# Decorators example
# Just because some cases are not obvious :o
# Note that the following examples work only for functions (and class for the
# last one).
# For decorators dedicated to classes method, take a look at method_decorator
# plugin in Github: github.com/denis-ryzhkov/method_decorator/

from __future__ import print_function

def basic_decorator(f):
    def wrapper(*args, **kwargs):
        print("This function is decorated.")
        print("Function args: {}".format(args))
        print("Function kwargs: {}".format(kwargs))
        rslt = f(*args, **kwargs)
        print("Finished")
        return rslt
    return wrapper

def with_params(*args, **kwargs):
    print("I'm evaluated on load.")
    deco_args = {'args': args, 'kwargs': kwargs}
    def realdecor(f):
        def wrapper(*args, **kwargs):
            print("This function is decorated.")
            print("Decorator args: {}".format(deco_args['args']))
            print("Decorator kwargs: {}".format(deco_args['kwargs']))
            print("Function args: {}".format(args))
            print("Function kwargs: {}".format(kwargs))
            rslt = f(*args, **kwargs)
            print("Finished")
            return rslt
        return wrapper
    return realdecor

class BasicClassDecorator(object):
    def __init__(self, f):
        self.f = f
        print("Init decorator.")

    def __call__(self, *args, **kwargs):
        print("Function called")
        print("Function args: {}".format(args))
        print("Function kwargs: {}".format(kwargs))
        rslt = self.f(*args, **kwargs)
        print("Finished")
        return rslt

class ClassDecoWithParams(object):
    def __init__(self, *params, **kwargs):
        self.args = params
        self.kwargs = kwargs
        print("Init decorator with params")
    def __call__(self, f):
        def wrapper(*args, **kwargs):
            print("This function is decorated.")
            print("Decorator args: {}".format(self.args))
            print("Decorator kwargs: {}".format(self.kwargs))
            print("Function args: {}".format(args))
            print("Function kwargs: {}".format(kwargs))
            rslt = f(*args, **kwargs)
            print("Finished")
            return rslt
        return wrapper

# Advanced system with two classes from https://stackoverflow.com/questions/10294014/python-decorator-best-practice-using-a-class-vs-a-function#10300995
class DecoratorBackWithParams(object):
    def __init__(self, f, *args, **kwargs):
        self.__doc__ = f.__doc__
        print("Init Back:", f, args, kwargs)
        self.f = f
        self.args = args
        self.kwargs = kwargs
    
    def __call__(self, *args, **kwargs):
        print("Call the function", args, kwargs)
        result = self.f(*args, **kwargs)
        print("Function called:", result)
        return result
        
class DecoratorFrontWithParams(object):
      def __init__(self, *args, **kwargs):
        print("Init Front:", args, kwargs)
        self.args = args
        self.kwargs = kwargs
      def __call__(self, f):
        print("Return Back instance.", f)
        return DecoratorBackWithParams(f, *self.args, **self.kwargs)

def basic_class_decorator(cls):
    """Truly. It's possible :3"""
    class Wrapper(cls):
        def added(self):
            print("I'm a method added by the decorator")
    return Wrapper

@basic_decorator
def earth(answer=42):
    print("The answer is {}".format(answer))

@with_params('yop', foo='toto')
def moon(status='crashed'):
    print("The moon is {}".format(status))

@BasicClassDecorator
def mars(water=False):
    print("There is{} water on Mars.".format('' if water else ' no'))

@ClassDecoWithParams(action='nothing')
def pluto(isplanet=False):
    print("Pluto is {} a planet".format('still' if isplanet else 'no more'))
    
@DecoratorFrontWithParams(distance='4.243 +/- 0.002 al')
def proxima_centauri(constellation='Centauri'):
    print(
        "The closest star from solar system, is inside {0} constellation".format(
            constellation))

@basic_class_decorator
class Sun(object):
    def __init__(self):
        print("I'm decorated")

if __name__ == '__main__':
    earth()
    moon()
    mars()
    pluto()
    sun = Sun()
    sun.added()
    proxima_centauri()
