#! /usr/bin/env python
# -*- coding: utf-8 -*-
#
# config.py
# A proper way to deal with config files.

import os

from ConfigParser import RawConfigParser as ConfigParser

# Build config parser
config = ConfigParser()

# Get config file paths
config_filepaths = ['/etc/myapp/conf.ini', '/opt/myapp/conf.ini']

env_config_filepath = os.environ.get('CONF')
if env_config_filepath is not None:
    config_filepaths.append(os.path.expanduser(env_config_filepath))

user_config_filepath = os.path.join(
    '/home',
    os.environ.get('USER'),
    '.config/myapp/conf.ini')
if os.path.isfile(user_config_filepath):
    config_filepaths.append(user_config_filepath)

# Load config
config.read(config_filepaths)

main = {k.upper(): v for k, v in config.items('main')}
# One line by bloc
