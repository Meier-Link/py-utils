#! /usr/bin/env python3

# @file neuron.py
# @brief Simple yet working example of a fully connected neural network in Python.
# @author <jeremie.balagna@gmail.com> Jeremie Balagna-Ranin
# @licence WTFPL (http://www.wtfpl.net/)

# Resources:
# * https://towardsdatascience.com/how-to-build-your-own-neural-network-from-scratch-in-python-68998a08e4f6
# * https://medium.com/@erikhallstrm/backpropagation-from-the-beginning-77356edf427d
# * https://towardsdatascience.com/under-the-hood-of-neural-networks-part-1-fully-connected-5223b7f78528

from collections import UserDict
from typing import Iterable, List, Sequence, Optional
from math import log

def normalize(values:Sequence[float], min_val:float, max_val:float) -> List[float]:
    """Normalize each item of the list based on given min and max.

    :param values: the list of values to normalize.
    :param min_val: the minimum possible value of the set.
    :param max_val: the maximum possible value of the set.
    :return list: the normalized list.
    """
    assert min_val <= min(values)
    assert max_val >= max(values)
    return [(float(x) - min_val) / (max_val - min_val)
        for x in values]


def denormalize(values:Sequence[float], min_val:float, max_val:float) -> List[float]:
    """Denormalize a the list based on given min and max.

    :param values: the list of values to drnormalize.
    :param min_val: the minimum possible value of the set.
    :param max_val: the maximum possible value of the set.
    :return list: the denormalized list.
    """
    return [(float(x) * (max_val - min_val)) + min_val
        for x in values]

class Neuron:
    """A simple neuron, with input, output and a function on
        the middle.

        NB: subclass this class to implement algorithms.
    """
    def __init__(self, weight:float, bias:float):
        """Create a neuron.

            :param weight: the weight applied on activation.
            :param bias: the bias of the neuron output.
        """
        self._w = weight
        self._b = bias
        self._result:Optional[float] = None

    @property
    def result(self) -> Optional[float]:
        return self._result

    def _activate(self, data:float, data_set:Sequence[float]) -> float:
        """Activation function.

            Some activation functions like softmax needs the full data set to operate.
            :param data: the value on which we apply activation function.
            :param data_set: the full data set for activation functions which need it.
            :return float: the result of the activation function.
        """
        return self._w * data

    def _loss(self, expected:float) -> float:
        """Calculate error on the result.

            Override here to change loss function.

            :param expected: what was the expected result for this neuron.
            :return float: the loss of the neuron.
        """
        # Root Mean Square Error :
        #return expected * log(self._result)
        # Simplest way:
        assert self._result is not None, (
            'Cannot calculate loss if no data has been processed'
        )
        return expected - self._result

    def _expected_input(self, expected:float) -> float:
        """Reverse the process equation.

            :param expected: the expected data of the current neuron.
            :return float: what the neuron expect as input.
        """
        return (self._b - expected) / self._w # XXX w can be 0?

    def process(self, data:Sequence[float]):
        """Perform calculation of output.
        :param data: the data to process. """
        activated = sum([self._activate(d, data) for d in data]) / len(data)
        self._result = activated + self._b

    def update(self, expected:float, learning_rate:float) -> float:
        """Neuron learns

            :param expected: what was expected as result.
            :param learning_rate: the learning rate common to the whole network.
            :return: what the neuron expects as input.
        """
        loss = self._loss(expected)
        self._w = self._w + (loss * learning_rate)
        self._b = self._b + (loss * learning_rate)
        return self._expected_input(expected)

    def copy(self) -> 'Neuron':
        """Create a copy of this neuron."""
        return self.__class__(self._w, self._b)

    def __repr__(self) -> str:
        """Show current state of weight and bias."""
        return "[" + repr(self._w) + "," + repr(self._b) + "]"


class Layer:
    """A simple list of neurons"""

    def __init__(self, weight:Optional[float]=None, bias:Optional[float]=None,
            size:int=1, neuron:Optional[Neuron]=None):
        """Create a new layer.

            :param weight: the weight of the neurons in this layer.
            :param bias: the bias of the neurons in this layer.
            :param size: how many neurons in this layer.
            :param neuron: customize the neuron class to override the default math
            functions.
        """
        if neuron is None:
            # NB: we have still issues with weight == 0 (see Neuron class above)
            assert weight is not None and weight != 0 and bias is not None
            neuron = Neuron(weight, bias)
        self._neurons = [neuron.copy() for _ in range(size)]

    @property
    def neurons(self) -> List[Neuron]:
        """Give the list of neurons from this layer."""
        return self._neurons

    def process(self, data_set:Sequence):
        """Hidden and output layers submit all the data set
            to each neuron.

            NB: a more subtle system makes possible to select which part of the
            set to submit to which neuron. But here is a fully connected
            network.

            :param data_set: the set of data which is processed by each neuron
            of this layer.
        """
        for n in self._neurons:
            n.process(data_set)

    def update(self, expected:Sequence[float], learning_rate:float) -> List[float]:
        """Update the weight and bias of this neurons' layer.

            :param expected: the expected output of each neuron.
            :param learning_rate: the network's learning rate.
            :return list: what's required by the neurons of this layer to reach
            their expected output.
        """
        assert len(expected) == len(self._neurons)
        hoped = []
        for n, e in dict(zip(self._neurons, expected)).items():
            hoped.append(n.update(e, learning_rate))
        return hoped

    def __str__(self) -> str:
        """String representation of this layer."""
        return '(' + ','.join([str(x.result) for x in self._neurons]) + ')'

    def __repr__(self) -> str:
        """Show current state of the layer's neurons."""
        return "[" + ','.join([repr(x) for x in self._neurons]) + "]"


class InputLayer(Layer):
    def process(self, data_set:Sequence[float]):
        """Input layer provides each item to one neuron."""
        assert len(data_set) == len(self._neurons)
        for n, d in dict(zip(self._neurons, data_set)).items():
            n.process([d])


class LayerConfiguration(UserDict):
    """A convenient way to configure a layer."""

    def __init__(self, **kwargs):
        """Provide the parameters expected by a layer.

            :param weight: the weight of the neurons in the target layer.
            :param bias: the bias of the neurons in the tagret layer.
            :param size: how many neurons in the target layer.
            :param neuron: customize the neuron class to override the default math
            functions.

            Either neuron or (weight and bias) has to be provided.
            If weight and bias are provided, the default neuron class is used.
            The neuron argument can be provided to use a custom neuron class.
        """
        keys = kwargs.keys()
        assert 'neuron' in keys or ('weight' in keys and 'bias' in keys)
        self.data = kwargs

    def __missing__(self, k:str):
        if k not in ('weight', 'bias', 'size', 'neuron_class'):
            raise KeyError('{} is not an allowed key.'.format(k))
        return self[str(k)]

    def contains(self, k:str) -> bool:
        return str(k) in self.data

    def __setitem__(self, k:str, v):
        if k not in ('weight', 'bias', 'size', 'neuron_class'):
            raise KeyError('{} is not an allowed key.'.format(k))
        self.data[k] = v

    def __mul__(self, m:int) -> List['LayerConfiguration']:
        """Duplicate this layer conf m times."""
        # Most of the pythonistas will hate me for that >_<
        assert isinstance(m, int)
        return [self.__class__(**self) for _ in range(m)]

    def __add__(self, a) -> List:
        """Create a list with self and the given LayerConfiguration instance."""
        # Most of the pythonistas will hate me also for that >_<
        if isinstance(a, List):
            assert all(isinstance(x, LayerConfiguration) for x in a)
            return [self] + a
        else:
            assert isinstance(a, LayerConfiguration)
            return [self, a]

    def __radd__(self, a) -> List:
        """Create a list with self and the given LayerConfiguration instance."""
        # Most of the pythonistas will hate me also for that >_<
        if isinstance(a, List):
            assert all(isinstance(x, LayerConfiguration) for x in a)
            return a + [self]
        else:
            assert isinstance(a, LayerConfiguration)
            return [a, self]


class Network:
    """A simple list of layers, but with subtleties."""

    def __init__(self, layers_conf:Sequence[LayerConfiguration],
            learning_rate:float):
        """Create a network of neurons.

            :param layers_conf: the configuration of each layer.
            :param learning_rate: the learning rate of this network.
        """
        assert len(layers_conf) > 0
        self._lr = learning_rate
        self._layers:List[Layer] = [InputLayer(**layers_conf[0])]
        for layer_conf in layers_conf[1:]:
            self._layers.append(Layer(**layer_conf))

    @property
    def layers(self) -> Sequence[Layer]:
        """The list of layers from this network."""
        return self._layers

    @property
    def results(self) -> List[float]:
        """The result of the last process iteration."""
        return [n.result for n in self._layers[-1].neurons]

    def process(self, data_set:Sequence[float]):
        """Process the input data set.

            :param data_set: the data set to process.
        """
        for l in self._layers:
            l.process(data_set)
            data_set = [n.result for n in l.neurons]

    def update(self, expected:Sequence[float]):
        """Update the network according to its expected output.

            :param expected: the expected output of the network.
        """
        for l in self._layers[::-1]:
            expected = l.update(expected, self._lr)

    def __str__(self) -> str:
        """String representation of this network."""
        return '(' + ',\n '.join([str(x) for x in self._layers]) + ')'

    def __repr__(self) -> str:
        """Show current state of the network's layers."""
        return "[" + ',\n '.join([repr(x) for x in self._layers]) + "]"


def train_neuron(n:Neuron, data:List[float], expected:float,
        learning_rate:float, iteration:int):
    """Train a single neuron to test it."""
    print("Train a neuron...")
    print("Data: {}, target:{}, learning rate: {}".format(data, expected,
            learning_rate))
    for it in range(iteration):
        print("Iteration n°", it)
        n.process(data)
        print("Result:", ne.result)
        ne.update(expected, learning_rate)
    print("Done.")


def train_layer(l:Layer, data:List[float], expected:List[float],
        learning_rate:float, iteration:int):
    """Train a single layer to test it."""
    print("Train a layer...")
    print("Data: {}, target:{}, learning rate: {}".format(data, expected,
            learning_rate))
    max_in = max(data)
    min_in = min(data)
    data = normalize(data, min_in, max_in)
    min_out = min(expected)
    max_out = max(expected)
    expected = normalize(expected, min_out, max_out)
    for it in range(iteration):
        print("Iteration n°", it)
        l.process(data)
        result = denormalize(
            list(n.result for n in l.neurons),
            min_out,
            max_out
        )
        print("Result:", result)
        l.update(expected, learning_rate)
    print("Done.")


def train_network(n:Network, data:List[float], expected:List[float], iteration:int):
    """Train a full network to test it."""
    print("Train a network...")
    print("Data: {}, target:{}".format(data, expected))
    max_in = max(data)
    min_in = min(data)
    data = normalize(data, min_in, max_in)
    min_out = min(expected)
    max_out = max(expected)
    expected = normalize(expected, min_out, max_out)
    for it in range(iteration):
        print("Iteration n°", it)
        n.process(data)
        result = denormalize(n.results, min_out, max_out)
        print("Result:", result)
        n.update(expected)
    print("Done.")


if __name__ == '__main__':
    print("Let's test our network ;-)")
    print("Create a neuron")
    ne = Neuron(1.1, 0.2)
    # Following parameters let a chance to see the neuron into
    # action:
    train_neuron(ne, [1], 2, 0.6, 8)
    train_neuron(ne, [1,2], 2, 0.6, 8)
    train_neuron(ne, [1,2,3], 2, 0.6, 8)

    il = InputLayer(1.1, 0.2, 3)
    train_layer(il, [1,2,3], [2,4,6], 0.6, 8)
    hl = Layer(1.1, 0.2, 3)
    train_layer(hl, [2,3,4], [3,5,7], 0.6, 8)
    # NB: clearly, it'll work better with normalized inputs.
    # input layer, then hidden layers, then output layer.
    layers_conf = LayerConfiguration(weight=1, bias=0.1, size=3) \
        + LayerConfiguration(weight=1, bias=0.1, size=3) * 3 \
        + LayerConfiguration(weight=1, bias=0.1, size=3)
    learning_rate = 0.4
    n = Network(layers_conf, learning_rate)
    print(n)  # None every where <-- normal.
    train_network(n, [2,1,3], [3,2,5], 12)
    print("Current network results:")
    print(n)
    print("Current network state:")
    print(repr(n))
    # Okay, that network is not that much efficient, but that's because I don't
    # have all the maths required to implement powerful neurons.
