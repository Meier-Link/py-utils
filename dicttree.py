#! /usr/bin/env python3.9
#
# dicttree.py
# Build a tree of dictionaries with some customisation
# Inspired by https://gist.github.com/hrldcpr/2012250

__author__ = 'Jeremie BR aka Meier Link <jeremie.balagna@gmail.com>'

from copy import deepcopy
from collections import defaultdict
from dataclasses import dataclass, field
from typing import Any

# TODO might be override ?
# operator.__index__
# operator.__(l|r)shift__ to move up/down some node ?
# look at __(del|(g|s)et)etitem__ with slice (py3k) or __(del|(g|s)et)slice__
# (py2k) to understand how does it work and how we can implement them here.
# operator.contains to implement a in b.
# operator.__(i?)contat__ to quickly add children
# Not sure that's useful, but __imul__ to implement 2 * tree (and tree * 2).


class ComparisonReport:
    """This store the result of a comparison.

    Comparison attributes are rendered as a tuple of:
    - origin object,
    - dist (aka remote) object,
    - a diff as boolean or list of differences.
    """
    def __init__(self, orig, dist, diff):
        """Init the report.

        :param orig: The object which is compared with a remote one.
        :param dist: The remote object of the comparison.
        :param diff: The comparison result which is basically a boolean,
            but an enhanced comparison tools can fill it with other objects.
        """
        self._orig = orig
        self._dist = dist
        self._diff = diff

    @property
    def report(self):
        return (self._orig, self._dist, self._diff)


@dataclass(order=True)
class Node:
    """Create a node which can be optionaly with a type restriction.

    :param obj: The object to store in this node
    """
    obj: Any

    def __str__(self):
        return str(self.obj)


class DictTree(defaultdict):
    def __init__(self, node=None):
        """Initialize the tree.

        :param node: the node which corresponds to this tree (because a tree
            may be considered as a node with children).
        """
        super().__init__(DictTree)
        if isinstance(node, DictTree):
            raise TypeError('node value cannot be a DictTree')
        self._node = Node(node)
        self._depth = 0

    def __setitem__(self, k, v):
        # Make sure we add a DictTree as child
        if not isinstance(v, DictTree):
            v = DictTree(v)
        if v.depth >= self._depth:
            self._depth = v.depth + 1

        return super().__setitem__(k, v)

    def __repr__(self):
        elems_repr = ""
        for k, v in self.items():
            elems_repr += "{}: {}, ".format(repr(k), repr(v))
        if elems_repr == "":
            elems_repr = "{}"
        else:
            elems_repr = elems_repr[:-2]
        return "{}(node={}, {})".format(self.__class__.__name__,
            repr(self.node), elems_repr)

    def __str__(self):
        elems_str = ""
        for k, v in self.items():
            elems_str += "{}: {}, ".format(str(k), str(v))
        if elems_str != "":
            elems_str = ", {}".format(elems_str[:-2])
        return "{" + "node: {}{}".format(str(self._node), elems_str) + "}"

    def cmp(self, dist, cmp_func):
        """Compare self with a remote tree.

        If the comparison return false, return false. Otherwise, compare
        children.
        If only one children return False, return False.
        If some key is available only on one tree, return False.

        :param dist: The remote tree to compare with self.
        :param cmp_func: The comparison function to use which must return
            True is cmp is successful, False otherwhise.

        :return bool: True if the comparison succeed everywhere in the tree,
            False otherwise.

        """
        if cmp_func(self.node, dist.node) is False:
            return False
        else:
            dist = deepcopy(dist)

            for k, v in self.items():
                if k not in dist.keys():
                    return False
                else:
                    cmp_rslt = self[k].cmp(dist.pop(k), cmp_func)

                if cmp_rslt is False:
                    return False

            if len(dist) > 0:
                return False

        return True

    def compare(self, dist, cmp_func=None, report_class=ComparisonReport):
        """Rich comparison algo.

        Instead of simply returning False if the test fails only on one node,
        this one will render a ComparisonReport tree.
        See ComparisonReport doc for its structure.

        :param dist: The remote tree to compare with self.
        :param cmp_func: (default: None) A function which must return a
            diff result which will be stored in the report_class.diff.
            If cmp_func is None, operator.eq (similar to a == b) is used.
        :param report_class: The report class used to store the result.
            It must be instance of ComparisonReport or provide the same kind
            of behavior.

        :return a tree of report_class instances.
        """
        cmp_func = cmp_func or operator.eq
        report_tree = self.__class__()
        report_tree.node = report_class(
            self.node,
            dist.node,
            cmp_func(self.node, dist.node))

        dist = deepcopy(dist)

        for k, v in self.items():
            if k not in dist.keys():
                report_tree[k] = self.compare(self[k], None, cmp_func,
                    report_class)
            else:
                report_tree[k] = self.compare(self[k], dist.pop(k), cmp_func,
                    report_class)

        if len(dist) > 0:
            return False

        return True

    def find(self, nodeval):
        """Return the key of the first childtree found with the provided value.

        This method return None if the value does not exists in the tree.
        Be careful: Python acceptsi None as a dict key.

        :param nodeval: The value of the node to find.

        :return: The key found or None.
        """
        for key, child in self.items():
            if child.node == nodeval:
                return key
        for child in self.values():
            rslt = child.find(nodeval)
            if rslt is not None:
                return rslt
        return None

    def find_all(self, nodeval):
        """Return all keys of childtree which have the same value as the provided one.

        :param nodeval: The value of the nodes to find.

        :return: The list of keys found or an empty list.
        """
        rslt = []
        for key, child in self.items():
            if child.node == nodeval:
                rslt.append(key)
        for child in self.values():
            rslt += child.find_all(nodeval)
        return rslt

    def find_node(self, nodekey):
        """Return the first childtree found with the provided value.

        This method return None if the value does not exists in the tree.

        :param nodekey: The key of the childtree to find.

        :return: The childtree found or None.
        """
        if nodekey in self.keys():
            return self[nodekey]
        for child in self.values():
            rslt = child.find_node(nodekey)
            if rslt is not None:
                return rslt
        return None

    def find_all_node(self, nodekey):
        """Return all keychildtree found with the provided value.

        :param nodekey: The key of the childtree to find.

        :return: The list of childtree found or an empty list.
        """
        rslt = []
        if nodekey in self.keys():
            rslt.append(self[nodekey])
        for child in self.values():
            rslt += child.find_all_node(nodekey)
        return rslt

    @property
    def depth(self):
        if self._depth == 0:
            for child in self.values():
                if child.depth >= self._depth:
                    self._depth = child.depth + 1
            if self._depth == 0:
                self._depth = 1
        return self._depth

    @property
    def node(self):
        return self._node.obj

    @node.setter
    def node(self, val):
        if isinstance(val, DictTree):
            raise TypeError('Node value cannot be a DictTree')
        self._node = Node(val)

    def add_path(self, path, sep='.', nodes=[]):
        """Build a path from a list or a formatted string.

        :param path: The path as a string or a list
        :param sep: (default '.') separator to split strings
            (ignored if path is a list)
        :param nodes: Values to affect as default for children.
            If the nodes list is shorter than path len, last nodes will be
            set to None.
            If the path len is shorter than the nodes list, remaining nodes
            will be ignored.
        """
        # Node checking is managed by Node class
        if isinstance(path, str) or isinstance(path, unicode):
            listpath = path.split(sep)
        elif isinstance(path, list):
            listpath = path
        else:
            raise TypeError('path must be str or list, {} given'.format(type(path)))

        nodes = nodes[::-1]  # pop starts from the end.
        for child in listpath:
            self = self[child]
            node = None
            if nodes:
                node = nodes.pop()
            self.node = node

if __name__ == '__main__':
    tree = DictTree("I'm your father")
    tree['son'] = DictTree("Noooooooooooo!")
    tree['daughter'] = DictTree("You don't say!")
    tree.node = {'firstname': 'Anakin', 'lastname': 'Skywalker', 'nickname': 'Darth Vader'}
    print(tree)
