# -*- codding: utf:8 -*-
#
# Some tests around class
# Bound for: Python 2.x

import json


class PrettyClass(object):
    def __init__(self):
        """Init some attributes"""
        self.foo = "toto"
        self.bar = "tata"
        self.baz = list(["titi", "tutu"])
        self.the_answer = 42
        self.the_question = None

    def __str__(self):
        """Print Attributes of my class as a json output (only attributes
        initalized in __init__)"""
        return json.dumps(self.__dict__)


class Prettified(PrettyClass):
    def __init__(self):
        """Override PC values"""
        self.foo = 0
        self.bar = 1
        self.baz = [2, 3, 4, 5]


if __name__ == '__main__':
    pc = PrettyClass()
    print pc
    print ""
    p = Prettified()
    print p
