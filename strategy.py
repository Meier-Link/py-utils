# -*- coding: utf-8 -*-

import sys

class Finder(object):
    '''Strategy with generic operation'''

    def __init__(self, func=None):
        '''Provide function specific to your strategy'''
        if func is not None:
            self.execute = func

    def __call__(self, **kwargs):
        print '''Default operation on: {}'''.format(kwargs)

def find_flickr(uname, upwd, image):
    print '''Find {} on Flickr with uname={}, upwd={}'''.format(image, uname, upwd)

def find_local(path, filename):
    print '''Locate {} localy in {}'''.format(filename, path)

if __name__ == '__main__':
    args = sys.argv[1:]
    dest = args[0]
    opts = args[1:]

    if dest in ('flickr', 'local'):
        fname = 'find_{}'.format(dest)
        finder = Finder(locals()[fname])
        finder(opts)

