# -*- coding: utf-8 -*-
#
# author: Meier Link (jeremie.balagna@gmail.com)

from os import getenv, getcwd
from sys import exit
from os.path import join
from copy import deepcopy
from ConfigParser import SafeConfigParser
from argparse import ArgumentParser


class Config(dict):
    def __init__(self):
        self.cfgfile = "mytool.cfg"
        self.description = "This is a tool ... Or not, there is no tool here x)"
        self.epilog = "You can run it, but nothing will be done :p"
        
        self.args = {
            'the_path': {'short': 'p', 'default': './', 'help': 'Store the path'},
            'name': {'short': 'n', 'help': 'A name'},
            'enabled': {'short': 'e', 'action': 'store_true'},
            'optional': {'default': None, 'help': 'an option that can be empty'},
            'my_list_values': {'default': 'val1,val2,val3', 'help': 'comma separated list of options'}
        }

        self.cfg_paths = [
            '/usr/share/mytool/',
            join(getenv('HOME'), '.config/mytool'),
            getcwd()
        ]
        self._cli_vals = {}

    def _from_file(self):
        """ Parse from config files """
        cfg = SafeConfigParser(self.default_conf, allow_no_value=True)

        for p in self.cfg_paths:
            cfg.read(join(p, self.cfgfile))

        if cfg.has_section('global'):
            file_cfg = {o:v for o, v in cfg.items('global')}
            for k in file_cfg:
                if cfg.get('global', k) is not None:
                    try:
                        file_cfg[k] = cfg.getboolean('global', k)
                    except ValueError, e:
                        continue
        else:
            exit("[ERROR] Mandatory 'global' section not provided")

        self.update((k, v) for k, v in file_cfg.iteritems() if v or None)
        print "File conf: {}".format(self)

    def _from_cli(self):
        # Override from command line
        parser = ArgumentParser(description=self.description, epilog=self.epilog)
        args = deepcopy(self.args)
        for arg, opts in args.items():
            arg = '--{}'.format(arg.replace('_', '-'))
            if 'short' in opts:
                short = opts.pop('short')
                arg = (arg, '-{}'.format(short))
                #parser.add_argument('-{}'.format(short), **opts)
                parser.add_argument(*arg, **opts)
            else:
                parser.add_argument(arg, **opts)

        args_cfg = {k:v for k,v in vars(parser.parse_args()).items()}
        print "Args conf: {}".format(args_cfg)
        
        self._cli_vals = args_cfg
        print "Final conf: {}".format(args_cfg)

    def process(self, cli=False):
        if cli:
            self._from_cli()
        self._from_file()
        self.update((k, v) for k, v in self._cli_vals.iteritems() if v or None) 

    def __getitem__(self, key):
        # Override this method if you want to customise the value returned by your
        # config dict
        val = dict.__getitem__(self, key)
        if key == 'my_list_values':
            if isinstance(val, list):
                return val
            else:
                self[key] = val.split(',')
        return val

    def __str__(self):
        # almost the same as for __getitem__
        if self['my_list_values'] and not isinstance(self['my_list_values'], list):
            self['my_list_values'] = self['my_list_values']
        return super(Config, self).__str__()



class Runner(object):
    def __init__(self, origin):
        self.res = None
        config = Config()
        #config.from_file()
        #if origin == '__main__':
        #    config.from_cli()
        cli = True if origin == '__main__' else False
        config.process(cli)
        self.config = config

    def __call__(self):
        print "Run your tool here"
        print "Config: {}".format(self.config)

run = Runner(__name__)
if __name__ == '__main__':
    run()
