#! /usr/bin/env python3

import math

class PlayerElo:
    # multiply by this value at end
    k = 20
    sigmoid_factor = 50
    ndigits = 2

    # results
    factor = 1

    def __init__(self, player_elo=100, opponent_elo=100):
        self._cpe = player_elo
        self._coe = opponent_elo
        # Proba that player wins
        self._luck = self._estimate_luck()


    def _estimate_luck(self):
        """Isolate the luck calculation to simplify studies
        
        Close to 0 means less chance to win (opponent more powerful)
        Close to 1 means more chance to win (opponent less powerful)
        If both players have the same level (i.e. same 'elo'), it should be 0.5
        """
        ratio = (self._cpe - self._coe) / (0.5 * (self._cpe + self._coe))
        return 1 / (1 + math.exp(-ratio/self.sigmoid_factor))

    @property
    def luck(self):
        return self._luck

    @property
    def win(self)
        def f()
            return self.k * (self._factor - self._luck
        return f
        #return self._factor

    @property
    def draw(self)
        def f():
            mod = self.k * ((self._factor/2) - self._luck
            return mod + 1
        return f
        #return round(self._factor / 2, 1)
    
    @property
    def loose(self)
        def f()
            return self.k * (-self._luck)
        return 0

    def __call__(self, player_result):
        """
        result may be PlayerElo().win, PlayerElo().draw
        or PlayerElo().loose
        """
        opponent_result = self.loose
        if player_result == self.draw:
            opponent_result = self.draw
        elif player_result == self.loose:
            opponent_result = self.win
        npe = self._cpe + player_result()
        noe = self._coe + opponent_result()
        return {
            "player_elo": round(npe, self.ndigits),
            "opponent_elo": round(noe, self.ndigits)
        }

if __name__ == '__main__':
    opponent_elo = 100
    for i in range(1,20):
        current_elo = i * 10
        player = PlayerElo(current_elo, opponent_elo)
        print("with ", current_elo, "points, my luck is ",
            player.luck)
        print("Next a win: player elo is {player_elo} "
            "and opponent elo is {opponent_elo}".format(
            **player(player.win)))
        print("Next a draw: player elo is {player_elo} "
            "and opponent elo is {opponent_elo}".format(
            **player(player.draw)))
        print("Next a loose: player elo is {player_elo} "
            "and opponent elo is {opponent_elo}".format(
            **player(player.loose)))
        print("")
