#! /usr/bin/env python3
#-*-coding:utf8-*-

from collections import namedtuple
from collections.abc import Callable
from uuid import uuid4
from math import log, exp


## What I understood ##

# Each neuron has a weight and a bias.
# A neuron receives inputs from 1..n neurons of the previous layer and forward
# its output to 1..n neurons of the next layer.

# The same activation function applies to all the neurons of a given layer.
# The activation function is applied to the sum of the weighted inputs and
# (it seems that) bias is applied next activation to give the output.
# The activation function should be derivable.
# The loss function is the derivative of the activation function applied to
# the difference between expected output and found output of the neuron.
# The back propagation can be summarized as:
# new_val = learning_rate * loss * current_val
# This formula is applied to update both weight and bias of all the neurons of
# the current layer.

# So a given layer provides the activation function, the loss function and the
# back propagation function.

# NB: for an inner layer, we might be able to calculate the expected output of its
# neurons based on the new weight, new bias and expected output of the above
# layer (to dig).

# For convenience:
# * a neuron of a given layer 'subscribe' to the output of the neurons of the
# previous layer;
# * a given layer knows who is the previous layer (None for the input layer);
# * the input layer may normalize the input of its neurons to simplify further
# operations;
# * the output layer may denormalize the output of its neurons to get back
# expected magnitude of values ('ordre de grandeur');
# * the LayerFunction class will have ReLU and its derivative as default
# activation and loss functions, but will accept alternative one on __init__.


class Neuron:
    """Representation of a single neuron in the network.

        A neuron belongs to a layer of the network.
        Each neuron has a weight and a bias. The weight is applied on each input of
        the neuron, and the bias is applied to the output of the neuron, before its
        transmission.
        An activation function is applied to the weighted inputs of the neuron.
        That function is the same for all the neurons of a given layer.
        A neuron receives inputs from 1..n neuron of the previous layer (except for
        the neurons of the input layer) and fowrard its output to 1..n neurons of
        the next layer (except for the neurons of the output layer).
    """
    def __init__(self, activation_function:Callable, weight:float=1.0,
            bias:float=0.1):
        """Initialize the neuron instance

            :param activation_function: The function used for activation of
            this neuron. The function should accept the list of inputs, the
            weight and the bias of the neuron.
            :param weight: The weight applied to the each input of the neuron.
            :param bias: The bias applied to the output of the neuron before it
            is sent to the followers.
        """
        self._af = activation_function
        self._w = weight
        self._b = bias
        # A convenient uniq id to identify a neuron in the network.
        self._id = str(uuid4())
        # List of neurons (uid) which has given their input.
        self._received_inputs = []
        # To which neurons the result should be forwarded
        self._targets = []
        # Weighted result given by each neuron.
        self._results = []
        # The final result to be forwarded (`None` means nothing happened yet).
        self._result = None

    @property
    def weight(self) -> float:
        """Weight of the current neuron."""
        return self._w

    @weight.setter
    def weight(self, w:float):
        """Update the weight of the current neuron.

            See backpropagation process on the layer level.

            :param w: The new weight to apply to this neuron.
        """
        self._w = w

    @property
    def bias(self) -> float:
        """Bias of the current neuron."""
        return self._b

    @bias.setter
    def bias(self, b:float):
        """Update the bias of the current neuron.

            See backpropagation process on the layer level.

            :param w: The new bias to apply to this neuron.
        """
        self._b = b

    @property
    def result(self) -> float:
        """Each neuron exposes the result of its process for convenience."""
        return self._result

    def subscribe(self, neuron):
        """Convenient way to let a neuron tells that it expects to receive
            results of this one.

            :param neuron: The neuron which wants to subscribe to the process'
            result.
        """
        self._targets.append(neuron)

    def process(self, in_data:float, in_id:str=None):
        """Process the inputs neurons result.

            The neuron receives input data from 1..n neurons of the previous
            layer.
            When all the inputs are received, it applies:
            * its weight on them,
            * the activation function given by its layer, and
            * its bias.

            When all those operations are done, the neuron forwards its result
            to the 1..n neurons of the next layer which subscribed to it.

            :param in_data: The result from a single neuron.
            :param in_id: Id of the neuron which submit its output. The neurons
            of the input layer receives `None` here.
        """
        if in_id not in self._received_inputs:
            self._received_inputs.append(in_id)
            self._results.append(in_data * self._w)

        if len(self._received_inputs) == max(1, len(self._targets)):
            self._result = self._af(self._results) + self._b
            for neurons in self._targets:
                neuron.process(self._result, self._id)

class LayerFunctionSet:
    """Set of functions used for a given layer.

        For each layer,
        * the same activation function is applied to each neuron,
        * the loss function is the derivative of the activation function,
        * the backpropagation function depends on the loss of the given neuron.

        Those functions are applied for each neuron of the layer.

        Create subclass of this one to create new set of functions.
    """
    def __init__(self, learning_rate:float=0.1):
        """Setup the instance of the function set."""
        self._lr = learning_rate
        # Store the result of the loss - nothing loss since nothing processed.
        self._loss = None

    def activate(self, data) -> float:
        """Activation function to be submitted to the neuron of the current
            layer.

            :param data: The list of data received by the neuron.
        """
        # As default, the simplest form simply returns the sum of the inputs
        # divided by the amount of inputs (i.e. f(x) = ax+b where a is weight
        # and b is bias).
        return sum(data) / len(data)

    def loss(self, out_neurons:list, expected_results:list):
        """The loss function used to know which adjustment to apply to weight
            and bias.

            :param out_neurons: the list of neurons' outputs of the layer.
            :param expected_results: the expected outputs of those neurons.
            :param weight: the weight of this layer's neurons.
        """
        if not len(out_neurons) == len(expected_results):
            raise Exception('{} != {}'.format(
                len(out_neurons), len(expected_results)))
        losses = []
        for i in range(len(out_neurons)):
            losses.append(expected_results[i] - out_neurons[i])
        self._loss = -sum(losses) / len(losses)

    def back_propagate(self, current_value:float):
        """Back propagation to apply to the current value of either weight
            or bias.

            :param current_value: The value to update regarding the learning
            rate and the loss.
        """
        return self._lr * self._loss * current_value


class ReluSet(LayerFunctionSet):
    """ReLU variant of the function set."""
    def activate(self, data):
        """Activation function to run on the neuron input to obtain its
            output.

            As an example, we apply ReLU function.
        """
        # The function to use here should be derivable.
        processed = sum(data) / len(data)
        return 0 if processed < 0 else processed

    def loss(self, found_results, expected_results):
        """Executed once on the final layer to obtain the error regarding the
            target."""
        if not len(found_results) == len(expected_results):
            raise Exception('{} != {}'.format(
                len(found_results), len(expected_results)))
        self._loss = -sum(
            [
                expected * log(float(found)) \
                for expected, found \
                in dict(
                    zip(expected_results, found_results)
                ).items()
            ]
        )


class SoftmaxSet(ReluSet):
    """Activate using softmax instead of ReLU"""
    # TODO(JBR) retreive and put the real loss function for softmax.
    def activate(self, data):
        ex = [exp(x-max(data)) for x in data]
        return sum([x / sum(ex) for x in ex])


class Layer:
    """Represent a layer in the network.

        Following data is common to all the neuron of the layer:
        * weight,
        * bias,
        * activation function,
        * loss,
        * backpropagation function.
    """
    def __init__(self, num_neuron:int, layer_functions:LayerFunctionSet,
            neuron_weight:float=1, neuron_bias:float=0, prev_layer=None):
        """Setup a new layer.

            :param num_neuron: (int) the number of neurons to set in this layer
            :param layer_function: (LayerFunction) the math functions to use
            :param neuron_weight: (int) the weight of those neurons
            :param neuron_bias: (int) the bias applied to those neurons
            :param af: (Callable) the activation function used by those neurons.
        """
        self._pl = prev_layer
        self._lf = layer_functions
        self._neurons = []
        for _ in range(num_neuron):
            n = Neuron(layer_functions.activate, neuron_weight, neuron_bias)
            if prev_layer:
                for pn in prev_layer.neurons:
                    pn.subscribe(n)  # NB: not always to all (simplification)
            self._neurons.append(n)

    @property
    def neurons(self) -> list:
        """Convenient way to retreive the neurons of the current layer."""
        return self._neurons

    def back_propagate(self):
        for n in self._neurons:
            if n.result is not None:
                n.weight = self._lf.back_propagate(n.weight)
                n.bias = self._lf.back_propagate(n.bias)
        if self._pl:
            self._pl.back_propagate()

    def loss(self, expected_results):
        self._lf.loss([x.result for x in self._neurons], expected_results)
        # TODO(JBR) Calculate the loss of the previous layer if any.
        # So we have to retreive the expected result of it based on the
        # expected result and updated values of weight and bias of current
        # layer's neurons.
        # It means that, if activation function gives y = ax+b, the function
        # which gives the expted result of the previous layer is x = (y - b) / a
        # NB: It should be one of the functions stored in the LayerFunctionSet


class InputLayer(Layer):
    """Input layer receives input data"""
    def run(self, inputs):
        if not len(inputs) == len(self._neurons):
            raise Exception('{} != {}'.format(
                len(inputs), len(self._neurons)))
        for idx in range(len(inputs)):
            self._neurons[idx].process(inputs[idx])


LayerConfiguration = namedtuple(
    'LayerConfiguration',
    [
        'size',  # < The number of neuron of the layer.
        'weight',  # < The weight of those neurons.
        'bias',  # < The bias of those neurons.
        'function_set'  # < the LayerFunctionSet used for this layer.
    ]
)


class Network:
    """Represents the entire network we are looking for."""
    def __init__(self,
            learning_rate:float,
            number_hidden_layer:int,
            expected_results:list,
            input_layer:LayerConfiguration,
            output_layer:LayerConfiguration,
            hidden_layers:LayerConfiguration=None):
        """Setup the network with the layers configuration.

            :param learning_rate: the learning rate applied to each layer.
            :param number_hidden_layer: tells how many hidden layer in this network.
            :param expected_results: the expected result for learning purpose.
            :param input_layer: the configuration for the input layer.
            :param output_layer: the configuration for the output layer.
            :param hidden_layers: the configuration for the hidden layers.
        """
        if len(expected_results) != output_layer.size:
            raise Exception('We should have that much item in the exptected result'
                'as the number of neurons in the output layer.')
        self._er = expected_results
        self._in_layer = InputLayer(
            input_layer.size,
            input_layer.function_set(learning_rate),
            input_layer.weight,
            input_layer.bias
        )
        pl = self._in_layer
        self._hidden_layers = []
        for x in range(number_hidden_layer):
            if x > 0:
                pl = self._hidden_layers[-1]
            hl = Layer(
                hidden_layers.size,
                hidden_layers.function_set(learning_rate),
                hidden_layers.weight,
                hidden_layers.bias,
                prev_layer=pl
            )
            self._hidden_layers.append(hl)
        self._out_layer = Layer(
            output_layer.size,
            output_layer.function_set(learning_rate),
            output_layer.weight,
            output_layer.bias,
            prev_layer=pl
        )

    def run(self, inputs:list):
        """Execute the network calulation on the given input.

            :param inputs: the data to feed the network. Its lenght must be
            equal to tne number of neurons in the input layer.
        """
        self._in_layer.run(inputs)

    def learn(self):
        self._out_layer.loss(self._er)
        self._out_layer.back_propagate()


if __name__ == '__main__':
    learning_rate = 0.1
    params = {
        'learning_rate': 0.1,
        'number_hidden_layer': 3,
        'expected_results': [3, 2, 1],
        'input_layer': LayerConfiguration(size=3, weight=1, bias=2,
            function_set=ReluSet),
        'hidden_layers': LayerConfiguration(size=3, weight=1, bias=2,
            function_set=ReluSet),
        'output_layer': LayerConfiguration(size=3, weight=1, bias=2,
            function_set=SoftmaxSet),
    }
    network = Network(**params)
